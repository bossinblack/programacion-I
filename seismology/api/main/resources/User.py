from flask_restful import Resource
from flask import request, jsonify
from .. import db
from main.models import UserModel
from flask_jwt_extended import (
    jwt_required,
    create_access_token,
    get_jwt_identity,
    jwt_optional,
)
from main.auth.decorators import admin_required


class User(Resource):
    # obtengo el resource
    @admin_required
    def get(self, id):
        user = db.session.query(UserModel).get_or_404(id)
        return user.to_json()

    # Para modificar el resource
    @admin_required
    def put(self, id):
        user = db.session.query(UserModel).get_or_404(id)
        # cree una variable para no escribirtanto
        info = request.get_json().items()
        for key, value in info:
            setattr(user, key, value)
        db.session.add(user)
        db.session.commit()
        return user.to_json(), 201

    # borrar un resource
    @admin_required
    def delete(self, id):
        user = db.session.query(UserModel).get_or_404(id)
        db.session.delete(user)
        db.session.commit()
        return "User was delete", 204


# resource usuario
class Users(Resource):
    @admin_required
    def get(self):
        page = 1
        per_page = 10
        users = db.session.query(UserModel)
        filters = request.get_json().items()

        for key, value in filters:
            # Paginacion
            if key == "page":
                page = int(value)
            if key == "per_page":
                per_page = int(value)

        users = users.paginate(page, per_page, True, 100)
        return jsonify(
            {
                "Users": [user.to_json() for user in users.items],
                "total": users.total,
                "pages": users.pages,
                "page": page,
            }
        )

    @admin_required
    def post(self):

        user = UserModel.from_json(request.get_json())
        exists = (
            db.session.query(UserModel).filter(UserModel.email == user.email).scalar()
            is not None
        )
        if exists:
            return "Duplicated mail", 409
        else:
            try:
                db.session.add(user)
                db.session.commit()

            except Exception as error:
                db.session.rollback()
                return str(error), 409
            return user.to_json(), 201
        db.session.add(user)
        db.session.commit()
        return user.to_json(), 201


class UsersPublic(Resource):
    def get(self):
        users = db.session.query(UserModel).all()
        return jsonify({"Users": [user.to_json_public() for user in users]})
