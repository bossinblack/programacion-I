from flask_restful import Resource
from flask import request, jsonify
from .. import db
from main.models import SensorModel
from main.models import UserModel
from flask_jwt_extended import jwt_required, get_jwt_identity
from main.auth.decorators import admin_required


class Sensor(Resource):

    # get para obtener el recurso
    @admin_required
    def get(self, id):
        sensor = db.session.query(SensorModel).get_or_404(id)
        return sensor.to_json()

    # para modificar el recurso
    @admin_required
    def put(self, id):
        sensor = db.session.query(SensorModel).get_or_404(id)
        data = request.get_json().items()
        for key, value in data:
            setattr(sensor, key, value)
        db.session.add(sensor)
        db.session.commit()
        return sensor.to_json(), 201

    # 201 me dice que tuve exito con la solicitud y la creacion

    # Elimino el recurso
    @admin_required
    def delete(self, id):
        sensor = db.session.query(SensorModel).get_or_404(id)
        db.session.delete(sensor)
        try:
            db.session.commit()
        except Exception as error:
            db.session.rollback()
            return "", 409
        return "Sensor was deleted", 204


class Sensors(Resource):
    # listo os recursos
    @admin_required
    def get(self):
        page = 1
        per_page = 10
        max_per_page = 20
        filters = request.get_json().items()
        sensors = db.session.query(SensorModel)
        for key, value in filters:
            # FILTROS

            if key == "name":
                sensors = sensors.filter(SensorModel.name.like("%" + value + "%"))
            if key == "active":
                sensors = sensors.filter(SensorModel.active == value)
            if key == "status":
                sensors = sensors.filter(SensorModel.status == value)
            if key == "userId":
                sensors = sensors.join(SensorModel.user).filter(UserModel.id == value)

            # ordenamiento
            if key == "sort_by":

                # el ordenamiento por el nombre
                if value == "name.asc":
                    sensors = sensors.order_by(SensorModel.name.asc())
                if value == "name.desc":
                    sensors = sensors.order_by(SensorModel.name.desc())
                # ordenamiento por el estatus
                if value == "status.asc":
                    sensors = sensors.order_by(SensorModel.status.asc())
                if value == "status.desc":
                    sensors = sensors.order_by(SensorModel.status.desc())
                # ordenamiento por activacion
                if value == "active.asc":
                    sensors = sensors.order_by(SensorModel.active.asc())
                if value == "active.desc":
                    sensors = sensors.order_by(SensorModel.active.desc())
                # ordenamiento por email o usuario asociado
                if value == "user.email.desc":
                    sensors = sensors.join(SensorModel.user).order_by(
                        UserModel.email.desc()
                    )
                if value == "user.email.asc":
                    sensors = sensors.join(SensorModel.user).order_by(
                        UserModel.email.asc()
                    )

            # paginacion
            if key == "page":
                page = int(value)
            if key == "per_page":
                per_page = int(value)

        sensors = sensors.paginate(page, per_page, True, max_per_page)
        return jsonify(
            {
                "Sensors": [sensor.to_json() for sensor in sensors.items],
                "total": sensors.total,
                "pages": sensors.pages,
                "page": page,
                "per_page": per_page,
            }
        )

    # inserto un recurso
    @admin_required
    def post(self):
        print(1)
        sensor = SensorModel.from_json(request.get_json())
        db.session.add(sensor)
        db.session.commit()
        print(2)
        return sensor.to_json(), 201


class SensorsPublic(Resource):
    def get(self):
        sensors = db.session.query(SensorModel).all()
        return jsonify({"Sensors": [sensor.to_json_public() for sensor in sensors]})
