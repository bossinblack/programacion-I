import time
from random import randint, uniform
from random import uniform, random, randint, uniform
from flask import request, jsonify
from flask_restful import Resource

from main.models import SeismModel
from main.models import SensorModel

from .. import db

from flask_jwt_extended import jwt_required, get_jwt_identity
from main.auth.decorators import admin_required


class Verifiedseism(Resource):

    # get para obtener el recurso
    # @jwt_required
    def get(self, id):
        seism = db.session.query(SeismModel).get_or_404(id)
        if seism.verified:
            return seism.to_json()
        else:
            return "Denied access", 403

    # antes daba error 404
    # ahora da error 403 que significa falta de permisos


class Verifiedseisms(Resource):
    def get(self):
        page = 1
        per_page = 10
        max_per_page = 20
        # filtros para sismos no verificados
        # Creo que con poner el sismoId tambien funcionaria
        filters = request.get_json().items()
        seisms = db.session.query(SeismModel).filter(SeismModel.verified == True)
        for key, value in filters:
            # filtro para el sensor.name
            if key == "sensorId":

                seisms = seisms.filter(SeismModel.sensorId == value)
            # filtro para fecha y hora
            if "datetime" in filters:
                seisms = seisms.filter(SeismModel.datetime == value)

            if key == "datetimede":
                seisms = seisms.filter(SeismModel.datetime >= value)

            if key == "datetimeTo":
                seisms = seisms.filter(SeismModel.datetime <= value)

            # Filtros para sensor.name

            # Filtros para magnitude
            if key == "magnitudeMax":
                seisms = seisms.filter(SeismModel.magnitude <= value)
            if key == "magnitudeMin":
                seisms = seisms.filter(SeismModel.magnitude >= value)
            # Filtros para depth
            if key == "depthMax":
                seisms = seisms.filter(SeismModel.depth <= value)
            if key == "depthMin":
                seisms = seisms.filter(SeismModel.depth >= value)

            # ORDENAMIENTO
            if key == "sort_by":
                # ordenamiento de fecha
                if value == "datetime.asc":

                    seisms = seisms.order_by(SeismModel.datetime.asc())
                if value == "datetime.desc":
                    seisms = seisms.order_by(SeismModel.datetime.desc())

                # Ordenamiento por magnitude
                if value == "magnitude.asc":
                    seisms = seisms.order_by(SeismModel.magnitude.asc())
                if value == "magnitude.desc":
                    seisms = seisms.order_by(SeismModel.magnitude.desc())
                # ordenamiento por profundidad DEPTH
                if value == "depth.asc":
                    seisms = seisms.order_by(SeismModel.depth.asc())
                if value == "depth.desc":
                    seisms = seisms.order_by(SeismModel.depth.desc())

                # Ordenamiento por nombre del sensorId
                if key == "sensor.name.asc":
                    seisms = seisms.join(SeismModel.sensor).order_by(
                        SeismModel.name.asc()
                    )

                if key == "sensor.name.desc":
                    seisms = seisms.join(SeismModel.sensor).order_by(
                        SeismModel.name.desc()
                    )
            # PAGINACION

            if key == "page":
                page = int(value)
            if key == "per_page":
                per_page = int(value)

        seisms = seisms.paginate(page, per_page, True, max_per_page)
        return jsonify(
            {
                "Verified-Seisms": [seism.to_json() for seism in seisms.items],
                "total": seisms.total,
                "pages": seisms.pages,
                "page": page,
            }
        )


class Unverifiedseism(Resource):
    # para modificar el recurso
    @jwt_required
    def get(self, id):
        seism = db.session.query(SeismModel).get_or_404(id)
        if not seism.verified:
            return seism.to_json()
        else:
            return "Denied access", 403

    @jwt_required
    def put(self, id):
        seism = db.session.query(SeismModel).get_or_404(id)
        data = request.get_json().items()
        if not seism.verified:
            for key, value in data:
                setattr(seism, key, value)
            db.session.add(seism)
            db.session.commit()
            return seism.to_json(), 201
        else:
            return "Denied access", 403

    # 201 me dice que tuve exito con la solicitud y la creacion

    # Elimino el recurso
    @jwt_required
    def delete(self, id):
        seism = db.session.query(SeismModel).get_or_404(id)
        if not seism.verified:
            db.session.delete(seism)
            db.session.commit()
            return "it (seism) was delete", 204
        else:
            return "Denied access", 403


class Unverifiedseisms(Resource):
    # listo os recursos
    @jwt_required
    def get(self):
        page = 1
        per_page = 10
        max_per_page = 20
        # filtros para sismos no verificados
        # Creo que con poner el sismoId tambien funcionaria
        filters = request.get_json().items()
        seisms = db.session.query(SeismModel).filter(SeismModel.verified == False)
        for key, value in filters:
            # filtro para el sensor.name
            if key == "sensorId":

                seisms = seisms.filter(SeismModel.sensorId == value)
            # filtro para fecha y hora
            if "datetime" in filters:
                seisms = seisms.filter(SeismModel.datetime == value)

            if key == "datetimede":
                seisms = seisms.filter(SeismModel.datetime >= value)

            if key == "datetimeTo":
                seisms = seisms.filter(SeismModel.datetime <= value)

            # Filtros para sensor.name

            # Filtros para magnitude
            if key == "magnitudeMax":
                seisms = seisms.filter(SeismModel.magnitude <= value)
            if key == "magnitudeMin":
                seisms = seisms.filter(SeismModel.magnitude >= value)
            # Filtros para depth
            if key == "depthMax":
                seisms = seisms.filter(SeismModel.depth <= value)
            if key == "depthMin":
                seisms = seisms.filter(SeismModel.depth >= value)

            # ORDENAMIENTO
            if key == "sort_by":
                # ordenamiento de fecha
                if value == "datetime.asc":

                    seisms = seisms.order_by(SeismModel.datetime.asc())
                if value == "datetime.desc":
                    seisms = seisms.order_by(SeismModel.datetime.desc())

                # Ordenamiento por magnitude
                if value == "magnitude.asc":
                    seisms = seisms.order_by(SeismModel.magnitude.asc())
                if value == "magnitude.desc":
                    seisms = seisms.order_by(SeismModel.magnitude.desc())
                # ordenamiento por profundidad DEPTH
                if value == "depth.asc":
                    seisms = seisms.order_by(SeismModel.depth.asc())
                if value == "depth.desc":
                    seisms = seisms.order_by(SeismModel.depth.desc())

                # Ordenamiento por nombre del sensorId
                if key == "sensor.name.asc":
                    seisms = seisms.join(SeismModel.sensor).order_by(
                        SeismModel.name.asc()
                    )

                if key == "sensor.name.desc":
                    seisms = seisms.join(SeismModel.sensor).order_by(
                        SeismModel.name.desc()
                    )
            # PAGINACION

            if key == "page":
                page = int(value)
            if key == "per_page":
                per_page = int(value)

        seisms = seisms.paginate(page, per_page, True, max_per_page)
        return jsonify(
            {
                "Unverified-Seisms": [seism.to_json() for seism in seisms.items],
                "total": seisms.total,
                "pages": seisms.pages,
                "page": page,
            }
        )

    @jwt_required
    def post(self):
        value_sensor = {
            "datetime": time.strftime(r"%Y-%m-%d %H:%M:%S", time.localtime()),
            "depth": randint(5, 250),
            "magnitude": round(uniform(2.0, 5.5), 1),
            "latitude": uniform(-180, 180),
            "longitude": uniform(-90, 90),
            "verified": False,
            "sensorId": 1,
        }

        seisms = SeismModel.from_json(value_sensor)
        # print("hoal")
        db.session.add(seisms)
        db.session.commit()
        # print("asd")
        return seisms.to_json(), 201
