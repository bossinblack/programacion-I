import time
import socket
import json


from .. import db
from ..models import SensorModel, SeismModel

# crea el socket
def create_socket():
    try:
        sen = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        set.settimeout(2)

        return sen

    except socket.error:
        print("Error to create socket")
        return None


# Checkea en que estado se encuentra el sensor
def check_sensor(id):
    sensor = db.session.query(SensorModel).get_or_404(id)
    sen = create_socket()
    if sen:
        sen.sendto(b" ", (sensor.ip, sensor.port))
        try:
            de = sen.recvfrom(1024)[0]
            sensor.status = True
            db.session.add(sensor)
            db.session.commit()
        except socket.timeout:
            print("Sensor" + sensor.name + "not found")


# Llama al sensor desde la app
def call_sensors(app):
    with app.app_context():
        sen = create_socket()
        while sen:
            sensors = (
                db.session.query(SensorModel)
                .filter(SensorModel.active == True)
                .filter(SensorModel.status == True)
                .all()
            )
            for sensor in sensors:
                sen.sendto(b" ", (sensor.ip, sensor.port))
                try:
                    de = sen.recvfrom(1024)[0]
                    seism = SeismModel.from_json_seism(json.loads(de))
                    seism.sensorId = sensor.id
                    seism.verified = False
                    db.session.add(seism)
                    db.session.commit()
                except socket.timeout:
                    sensor.status = False
                    db.session.add(sensor)
                    db.session.commit()
                    print("Sensor" + sensor.name + "not found")
            time.sleep(2)
