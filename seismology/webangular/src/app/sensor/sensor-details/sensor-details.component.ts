import { Component, Input, OnInit } from '@angular/core';
import { Sensor } from "../sensor.model"

@Component({
  selector: 'app-sensor-detail',
  templateUrl: './sensor-details.component.html',
  styleUrls: ['./sensor-details.component.scss']
})
export class SensorDetailsComponent implements OnInit {
  @Input() sensor: Sensor;
  constructor() { }

  ngOnInit(): void {
  }

}
