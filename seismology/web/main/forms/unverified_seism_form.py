from flask_wtf import FlaskForm
from wtforms import validators  # Importa validaciones
from wtforms.fields.html5 import DateTimeLocalField as DateTimeField
from wtforms import (
    BooleanField,
    FloatField,
    IntegerField,
    SelectField,
    SubmitField,
)

# revisarlo con los formularios actuales


class UnverifiedSeismEdit(FlaskForm):
    # Definicion de campo Integer
    depth = IntegerField(
        label="Depth",
        validators=[validators.DataRequired(message="This field should be an integer")],
    )

    # Definicion de campo Float
    magnitude = FloatField(
        label="Magnitude",
        validators=[
            validators.DataRequired(message="This field should be a decimal value")
        ],
    )

    # Definicion de campo CheckBox
    verified = BooleanField()

    # Definicion de campo Sumbit
    submit = SubmitField("Send")


class UnverifiedSeismFilter(FlaskForm):
    datetimede = DateTimeField(
        label="From Datetime",
        format="%%Y-%m-%dT%H:%M",
        validators=[validators.optional()],
    )

    datetimeTo = DateTimeField(
        label="To Datetime",
        format="%Y-%m-%dT%H:%M",
        validators=[validators.optional()],
    )

    depthMax = IntegerField(
        label="Depth Max",
        validators=[validators.optional()],
    )

    depthMin = IntegerField(
        label="Depth Min",
        validators=[validators.optional()],
    )

    magnitudeMin = FloatField(
        label="Magnitude Min",
        validators=[validators.optional()],
    )

    magnitudeMax = FloatField(
        label="Magnitude Max",
        validators=[validators.optional()],
    )

    sensorId = SelectField(
        label="Sensor Associated",
        validators=[validators.optional()],
        coerce=int,
    )

    submit = SubmitField(
        label="Filter",
    )

    download = SubmitField(label="Download")
