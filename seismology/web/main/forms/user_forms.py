from flask_wtf import FlaskForm
from wtforms import BooleanField, PasswordField, SubmitField
from wtforms import validators
from wtforms.fields.html5 import EmailField

# heredo desde un formulario de flask por eso flaskform
class UserCreateForm(FlaskForm):
    # Definicion del campo del Email
    email = EmailField(
        label="E-mail",
        validators=[
            validators.Required(message=" El E-mail es requerido"),
            validators.Email(message="Formato no valido"),
        ],
    )

    password = PasswordField(
        label="Password",
        validators=[
            validators.Required(message="CONTRASEÑA POR OBLIGACION MANITO"),
            validators.EqualTo("confirm", message="La contraseña no coincide"),
        ],
    )
    # si la contraseña no coincide el sumbit(el formulario en si) no se envia por que las pas no coinciden
    confirm = PasswordField(" Repita la contraseña")

    admin = BooleanField("Admin")

    # sumbit es el campo para enviar todo
    submit = SubmitField("Enviar")


class UserEdit(FlaskForm):
    # Definicion de campo del emailField
    email = EmailField(
        label="E-mail",
        validators=[
            validators.Required(message="e-mail is require/el e-mail es requerido"),
            validators.Email(message="Format not valid/formato no valido"),
        ],
    )
    # Definicion de campo Admin
    admin = BooleanField("Admin")

    # Definicion de campo Sumbit
    submit = SubmitField("Send")
