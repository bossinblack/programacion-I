from flask_wtf import FlaskForm
from wtforms import PasswordField, SubmitField
from wtforms import validators
from wtforms.fields.html5 import EmailField


class LoginForm(FlaskForm):

    # Defino el campo del email
    email = EmailField(
        label="E-mail",
        validators=[
            validators.Required(message="E-mail is require"),
            validators.Email(message="Format not valid"),
        ],
    )

    # Definicion de campo Contraseña
    password = PasswordField(
        label="Password",
        validators=[
            validators.Required(),
        ],
    )

    # Definicion de campo Sumbit
    submit = SubmitField(label="Login")
