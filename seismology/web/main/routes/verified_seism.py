from flask import (
    Blueprint,
    render_template,
    current_app,
    redirect,
    url_for,
    request,
    flash,
    make_response,
)
from flask import blueprints
from werkzeug.utils import header_property
from flask_breadcrumbs import register_breadcrumb, default_breadcrumb_root
from ..utilities.functions import sendRequest
from ..forms.unverified_seism_form import UnverifiedSeismEdit, UnverifiedSeismFilter
import json, io, csv


verified_seism = Blueprint("verified_seism", __name__, url_prefix="/verified-seism")


@verified_seism.route("/")
@register_breadcrumb(verified_seism, ".", "Verified Seism")
def index():

    filter = UnverifiedSeismFilter(request.args, meta={"csrf": False})
    # en este sendrequest preguntar a juampi sobre el data
    r = sendRequest(method="get", url="/sensors-info")
    filter.sensorId.choices = [
        (int(sensor["id"]), sensor["name"]) for sensor in json.loads(r.text)["Sensors"]
    ]
    filter.sensorId.choices.insert(0, [0, "All"])
    data = {}
    # comienzo el filtrado
    # Valido el formulario de Filtros
    if filter.validate():
        # filtro de la fecha
        if filter.datetimede.data and filter.datetimeTo.data:
            if filter.datetimede.data == filter.datetimeTo.data:
                data["datetime"] = filter.datetimeTo.data.strftime("%Y-%m-%d %H:%M")
        if filter.datetimede.data != None:
            data["datetimede"] = filter.datetimede.data.strftime("%Y-%m-%d %H:%M")
        if filter.datetimeTo.data != None:
            data["datetimeTo"] = filter.datetimeTo.data.strftime("%Y-%m-%d %H:%M")

        # filtro de la profundidad o Depth
        if filter.depthMin.data != None:
            data["depthMin"] = filter.depthMin.data
        if filter.depthMax.data != None:
            data["depthMax"] = filter.depthMax.data

        # filtro de la magnitud o Magnitude
        if filter.magnitudeMin.data != None:
            data["magnitudeMin"] = filter.magnitudeMin.data
        if filter.magnitudeMax.data != None:
            data["magnitudeMax"] = filter.magnitudeMax.data

        # filtro de sensor por identificacion
        if filter.sensorId.data != None and filter.sensorId.data != 0:
            data["sensorId"] = filter.sensorId.data

    # Comienzo con el ORDENAMIENTO
    if "sort_by" in request.args:
        data["sort_by"] = request.args.get("sort_by", "")

    if "download" in request.args:
        if request.args.get("download", "") == "Download":
            code = 200
            # Comenzar por la primera pagina
            page = 1
            list_seisms = []
            # Recorrer hasta que no haya mas paginas
            while code == 200:
                data["page"] = page
                # Llamada a la api
                r = sendRequest(
                    method="get",
                    url="/verified-seisms",
                    data=json.dumps(data),
                )
                code = r.status_code
                if code == 200:
                    # Recorrer los sismos de la pagina y colocar los campos que se quieren agregar
                    for seism in json.loads(r.text)["Verified-Seisms"]:
                        element = {
                            "datetime": seism["datetime"],
                            "depth": seism["depth"],
                            "magnitude": seism["magnitude"],
                            "latitude": seism["latitude"],
                            "longitude": seism["longitude"],
                            "sensor.name": seism["sensor"]["name"],
                        }
                        # Agrega cada elemento a la lista
                        list_seisms.append(element)
                # Aumenta el numero de paginacion en 1
                page += 1

            # Inicializar para poder escribir en el buffer de memoria

            si = io.StringIO()
            # Inicializar el objeto que va a escribir el csv a partir de un diccionario
            # Pasa las claves del diccionario como cabecera
            fc = csv.DictWriter(si, fieldnames=list_seisms[0].keys())
            # Escribi la cabecera
            fc.writeheader()
            # Escribi las filas
            fc.writerows(list_seisms)

            # Crea una respuesta que tiene como contenido el valor dedl buffer
            output = make_response(si.getvalue())
            # Colocar cabeceras para que se descargue como un archivo
            output.headers["Content-Disposition"] = "attachment; filename=seisms.csv"
            output.headers["Content-type"] = "text/csv"
            # Devolver la salida
            return output

    # Numero de pagina
    if "page" in request.args:
        data["page"] = request.args.get("page", "")
    else:
        if "page" in data:
            del data["page"]

    # Obtenego los datos de la api para la tabla
    r = sendRequest(method="get", url="/verified-seisms", data=json.dumps(data))

    # si me da correcto abro un if para poder seguir
    if r.status_code == 200:
        verified_seisms = json.loads(r.text)["Verified-Seisms"]
        pagination = {}
        # cargo en el diccionario de la paginacion el total de las paginas
        # pagina por pagina
        pagination["total"] = json.loads(r.text)["total"]
        pagination["pages"] = json.loads(r.text)["pages"]
        pagination["current_page"] = json.loads(r.text)["page"]
        title = "Verified Seisms List"

        return render_template(
            "verified_seisms.html",
            title=title,
            verified_seisms=verified_seisms,
            filter=filter,
            pagination=pagination,
        )
    else:
        flash("Error en el filtrado", "danger")
        return redirect(url_for("verified_seism.index"))


@verified_seism.route("/view/<int:id>")
@register_breadcrumb(verified_seism, ".view", "View")
def view(id):
    r = sendRequest(method="get", url="/verified-seism/" + str(id))
    if r.status_code == 404:
        return redirect(url_for("verified_seism.index"))
    verified_seism = json.loads(r.text)
    title = "Verified Seism View"
    return render_template(
        "verified_seismView.html",
        title=title,
        verified_seism=verified_seism,
    )
