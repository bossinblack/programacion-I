import json

from flask import Blueprint, current_app, flash, make_response, redirect, url_for
from flask_breadcrumbs import register_breadcrumb
from flask_login import login_user, logout_user
import requests

from . import verified_seism

# from ..forms.login_form import LoginForm
from .auth import User

main = Blueprint("main", __name__, url_prefix="/")


@main.route("/")
@register_breadcrumb(main, "breadcrumbs", "Home")
def index():
    return redirect(url_for("verified_seism.index"))
