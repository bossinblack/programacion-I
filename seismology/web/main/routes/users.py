import json

# from web.main.routes.auth import admin_required
from ..forms.user_forms import UserCreateForm, UserEdit
from flask import (
    Blueprint,
    render_template,
    current_app,
    redirect,
    url_for,
    flash,
    request,
)
from flask import blueprints
from werkzeug.utils import header_property
from flask_breadcrumbs import register_breadcrumb, default_breadcrumb_root
from ..utilities.functions import sendRequest
import requests
from flask_login import login_required

# from .auth import admin_required

users = Blueprint("users", __name__, url_prefix="/users")


# @admin_required
# @login_required
@users.route("/")
@login_required
@register_breadcrumb(users, ".", "Users")
def index():
    data = {}
    # Numero de pagina
    if "page" in request.args:
        data["page"] = request.args.get("page", "")
    else:
        if "page" in data:
            del data["page"]
    r = sendRequest(method="get", url="/users", data=json.dumps(data), auth=True)

    if r.status_code == 200:
        users = json.loads(r.text)["Users"]

        pagination = {}
        pagination["total"] = json.loads(r.text)["total"]
        pagination["pages"] = json.loads(r.text)["pages"]
        pagination["current_page"] = json.loads(r.text)["page"]

        title = "Users List"
        # el primer user es la variable que esta en el template del html de user en el for el segundo users es la variable de aca arriba del index
        return render_template(
            "user.html",
            title=title,
            users=users,
            pagination=pagination,
        )
    else:
        return redirect(url_for("users.index"))


@users.route("/add", methods=["GET", "POST"])
def create():
    form = UserCreateForm()
    title = "Add user"
    if form.validate_on_submit():
        user = {
            "email": form.email.data,
            "password": form.password.data,
            "admin": form.admin.data,
        }
        # dumps googlearlo
        data = json.dumps(user)
        r = sendRequest(method="POST", url="/users", data=data)

        return redirect(url_for("users.index"))  # me direcicona a la tabla
    # me muestra el formulario
    return render_template("add_user.html", title=title, form=form)


@users.route("/edit/<int:id>", methods=["GET", "POST"])
@register_breadcrumb(users, ".edit", "Edit User")
def edit(id):
    form = UserEdit()
    if not form.is_submitted():
        r = sendRequest(method="get", url="/user/" + str(id), auth=True)
        if r.status_code == 404:
            flash("User not Found/Usuario no encontrado", "danger")
            return redirect(url_for("users.index"))
        user = json.loads(r.text)
        form.email.data = user["email"]
        form.admin.data = user["admin"]

    if form.validate_on_submit():
        user = {"email": form.email.data, "admin": form.admin.data}

        data = json.dumps(user)
        r = sendRequest(method="PUT", url="/user/" + str(id), data=data, auth=True)
        flash("User edited/Usuario Editado", "success")
        return redirect(url_for("users.index"))
    return render_template("userEdit.html", form=form, id=id)


@users.route("/delete/<int:id>")
def delete(id):
    r = sendRequest(method="delete", url="/user/" + str(id), auth=True)
    flash("User was delete", "danger")
    return redirect(url_for("users.index"))