from functools import wraps

from flask import flash, redirect, request, url_for
from flask_login import UserMixin, LoginManager, current_user
import jwt
from functools import wraps

from .. import login_manager

# Clase donde guarda los datos de los usuarios logeados
class User(UserMixin):
    def __init__(self, id, email, admin):
        self.id = id
        self.email = email
        self.admin = admin


# Este metodo indica a LoginManager como obtener los datos del usuario logueado
# Nosotros trabajando con JWT los datos los obtendremos de los claims del token (el decorador) del mismo
# que a sido guardado en una cookie en el navegador (browser)
@login_manager.request_loader
def load_user(request):
    # se verifica si la cookie se cargo correctamente
    if "access_token" in request.cookies:
        try:
            # Decodifica el token
            decoded = jwt.decode(request.cookies["access_token"], verify=False)
            user_data = decoded["user_claims"]
            # carga los datos del usuario
            try:
                user = User(user_data["id"], user_data["email"], user_data["admin"])
                return user
            except KeyError:
                return redirect(url_for("main.index"))

        except jwt.exceptions.InvalidTokenError:
            print("Invalid Token.")
        except jwt.exceptions.DecodeError:
            print("DecodeError.")
    return None


# Esta funcion sobreescribe el metodo al intentar ingresar a una ruta no autorizada
@login_manager.unauthorized_handler
def unauthorized_callback():
    flash("Debe iniciar sesion para continuar.", "warning")
    # Nos redirecciona a la pagina quecontiene el formulario de login
    return redirect(url_for("main.index"))


# Define la función de verificación de admin para las rutas
def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kws):
        if not current_user.admin:
            flash("Acceso restringido a administradores.", "warning")
            return redirect(url_for("main.index"))
        return fn(*args, **kws)

    return wrapper