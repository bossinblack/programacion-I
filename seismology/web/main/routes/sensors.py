from flask import (
    Blueprint,
    render_template,
    current_app,
    redirect,
    url_for,
    flash,
    request,
)
from werkzeug.utils import header_property
from flask_breadcrumbs import register_breadcrumb, default_breadcrumb_root
from ..utilities.functions import sendRequest
import requests, json
from flask_login import login_required
from .auth import admin_required
from ..forms.sensor_forms import SensorEditForm, SensorFilterForm

sensors = Blueprint("sensors", __name__, url_prefix="/sensors")


@sensors.route("/")
@login_required
@admin_required
@register_breadcrumb(sensors, ".", "Sensors")
def index():
    # Me carga los parametros de la url en el formulario
    # Con csrf = false elimino la proteccion para el formulario del filtrado
    filter = SensorFilterForm(request.args, meta={"csrf": False})
    # el data de r pasa un coso sin filtrado :V
    r = sendRequest(method="get", url="/users-info")

    filter.userId.choices = [
        (int(user["id"]), user["email"]) for user in json.loads(r.text)["Users"]
    ]

    filter.userId.choices.insert(0, [0, "All"])
    data = {}
    # Aplica los filtros y valido el formularios

    if filter.validate():
        if filter.userId.data != None and filter.userId.data != 0:
            data["userId"] = filter.userId.data
        if filter.name.data != None:
            data["name"] = filter.name.data
        if filter.status.data:
            data["status"] = filter.status.data
        if filter.active.data:
            data["active"] = filter.active.data
    # Realiza el ordenamiento

    if "sort_by" in request.args:
        data["sort_by"] = request.args.get("sort_by", "")

    # LA numeracion de la pagina(saber el numero)
    if "page" in request.args:
        data["page"] = request.args.get("page", "")
    else:
        if "page" in data:
            del data["page"]

    # obtengo los datos de la api, pero tengo que consultar porque
    # no estoy comprendiendo mucho la logica del dato data=json.dumps
    r = sendRequest(method="get", url="/sensors", data=json.dumps(data), auth=True)

    if r.status_code == 200:
        sensors = json.loads(r.text)["Sensors"]
        pagination = {}
        # cargo en el diccionario de la paginacion el total de las paginas
        # pagina por pagina
        pagination["total"] = json.loads(r.text)["total"]
        pagination["pages"] = json.loads(r.text)["pages"]
        pagination["current_page"] = json.loads(r.text)["page"]
        title = "Sensors"
        return render_template(
            "sensors.html",
            title=title,
            sensors=sensors,
            filter=filter,
            pagination=pagination,
        )
    else:
        flash("Error de filtros", "danger")
        return redirect(url_for("sensors.index"))


@sensors.route("/view/<int:id>")
@login_required
@admin_required
@register_breadcrumb(sensors, ".view", "Sensor")
def view(id):
    r = sendRequest(method="get", url="/sensor/" + str(id), auth=True)
    if r.status_code == 404:
        flash("Sensor not found/sensor no econtrado", "danger")
        return redirect(url_for("sensors.index"))
    sensor = json.loads(r.text)
    title = "Sensor View"
    return render_template(
        "sensorview.html",
        title=title,
        sensor=sensor,
    )


@sensors.route("/delete/<int:id>")
@login_required
@admin_required
def delete(id):
    r = sendRequest(method="delete", url="/sensor/" + str(id), auth=True)
    flash("Sensor was delete", "danger")
    return redirect(url_for("sensors.index"))


@sensors.route("/edit/<int:id>", methods=["GET", "POST"])
@login_required
@admin_required
@register_breadcrumb(sensors, ".edit", "Edit Sensor")
def edit(id):
    form = SensorEditForm()
    r = sendRequest(method="get", url="/users-info")
    users = [(item["id"], item["email"]) for item in json.loads(r.text)["Users"]]
    form.userId.choices = users
    form.userId.choices.insert(0, [0, "Select one user"])
    if not form.is_submitted():
        r = sendRequest(method="get", url="/sensor/" + str(id), auth=True)
        if r.status_code == 404:
            flash("Sensor not found", "danger")
            return redirect(url_for("sensors.index"))
        sensor = json.loads(r.text)

        # carga los datos
        form.name.data = sensor["name"]
        form.port.data = sensor["port"]
        form.ip.data = sensor["ip"]
        form.status.data = sensor["status"]
        form.active.data = sensor["active"]

        #
        # Load users
        try:
            for user_id, user_email in users:
                if sensor["user"]["id"] == user_id:
                    form.userId.data = user_id

        except KeyError:
            pass

    if form.validate_on_submit():
        sensor = {
            "name": form.name.data,
            "ip": form.ip.data,
            "port": form.port.data,
            "status": form.status.data,
            "active": form.active.data,
            "userId": form.userId.data,
        }
        data = json.dumps(sensor)
        r = sendRequest(method="put", url="/sensor/" + str(id), data=data, auth=True)
        flash("Sensor edited", "success")
        return redirect(url_for("sensors.index"))

    return render_template("sensorEdit.html", id=id, form=form)
