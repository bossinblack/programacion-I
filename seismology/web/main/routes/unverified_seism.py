from flask import (
    Blueprint,
    render_template,
    current_app,
    redirect,
    url_for,
    flash,
    request,
)
from flask import blueprints
from requests.api import get
from werkzeug.utils import header_property
from flask_breadcrumbs import register_breadcrumb, default_breadcrumb_root
from ..utilities.functions import sendRequest
import requests, json
from flask_login import login_required
from ..forms.unverified_seism_form import UnverifiedSeismEdit, UnverifiedSeismFilter

unverified_seism = Blueprint(
    "unverified_seism", __name__, url_prefix="/unverified-seism"
)


@unverified_seism.route("/")
@login_required
@register_breadcrumb(unverified_seism, ".", "Unverified Seism")
def index():
    filter = UnverifiedSeismFilter(request.args, meta={"csrf": False})
    # en este sendrequest preguntar a juampi sobre el data
    r = sendRequest(method="get", url="/sensors-info")
    filter.sensorId.choices = [
        (int(sensor["id"]), sensor["name"]) for sensor in json.loads(r.text)["Sensors"]
    ]
    filter.sensorId.choices.insert(0, [0, "All"])
    data = {}
    # comienzo el filtrado
    # Valido el formulario de Filtros
    if filter.validate():
        # filtro de la fecha
        if filter.datetimede.data and filter.datetimeTo.data:
            if filter.datetimede.data == filter.datetimeTo.data:
                data["datetime"] = filter.datetimeTo.data.strftime("%Y-%m-%d %H:%M")
        if filter.datetimede.data != None:
            data["datetimede"] = filter.datetimede.data.strftime("%Y-%m-%d %H:%M")
        if filter.datetimeTo.data != None:
            data["datetimeTo"] = filter.datetimeTo.data.strftime("%Y-%m-%d %H:%M")

        # filtro de la profundidad o Depth
        if filter.depthMin.data != None:
            data["depthMin"] = filter.depthMin.data
        if filter.depthMax.data != None:
            data["depthMax"] = filter.depthMax.data

        # filtro de la magnitud o Magnitude
        if filter.magnitudeMin.data != None:
            data["magnitudeMin"] = filter.magnitudeMin.data
        if filter.magnitudeMax.data != None:
            data["magnitudeMax"] = filter.magnitudeMax.data

        # filtro de sensor por identificacion
        if filter.sensorId.data != None and filter.sensorId.data != 0:
            data["sensorId"] = filter.sensorId.data

    # Comienzo con el ORDENAMIENTO
    if "sort_by" in request.args:
        data["sort_by"] = request.args.get("sort_by", "")

    # Numero de pagina
    if "page" in request.args:
        data["page"] = request.args.get("page", "")
    else:
        if "page" in data:
            del data["page"]

    # Obtenego los datos de la api para la tabla
    r = sendRequest(
        method="get", url="/unverified-seisms", data=json.dumps(data), auth=True
    )

    # si me da correcto abro un if para poder seguir
    if r.status_code == 200:
        unverified_seisms = json.loads(r.text)["Unverified-Seisms"]
        pagination = {}
        # cargo en el diccionario de la paginacion el total de las paginas
        # pagina por pagina
        pagination["total"] = json.loads(r.text)["total"]
        pagination["pages"] = json.loads(r.text)["pages"]
        pagination["current_page"] = json.loads(r.text)["page"]
        title = "Unverified Seisms List"

        return render_template(
            "unverified_seisms.html",
            title=title,
            unverified_seisms=unverified_seisms,
            filter=filter,
            pagination=pagination,
        )
    else:
        flash("Error en el filtrado", "danger")
        return redirect(url_for("unverified_seism.index"))


# tengo que agregarle el def view (id) para poder que me vaya devolviendo el numero de identificacion
@unverified_seism.route("/view/<int:id>")
@login_required
@register_breadcrumb(unverified_seism, ".view", "UnverifiedSeisms")
def view(id):
    r = sendRequest(method="get", url="/unverified-seism/" + str(id), auth=True)
    if r.status_code == 404:
        flash("Seism not found/Sismo no encontrado", "danger")
        return redirect(url_for("unverified_seism.index"))
    unverified_seism = json.loads(r.text)
    title = "Unverified Seism Details"
    return render_template(
        "unverified_seismView.html", title=title, unverified_seism=unverified_seism
    )


# defino la variable para eliminar esta no necesita ser admin yaque lo ven los simologos
@unverified_seism.route("/delete/<int:id>")
@login_required
def delete(id):
    r = sendRequest(method="delete", url="/unverified-seism")
    flash("Unverified Seims ", "danger")
    return redirect(url_for("unverified_seism.index"))


# testeando la funcion con prints porque tengo problemas con la base de datos
@login_required
@register_breadcrumb(unverified_seism, ".edit", "Edita Unverified Seismos")
@unverified_seism.route("/edit/<int:id>", methods=["GET", "POST"])
def edit(id):

    form = UnverifiedSeismEdit()
    if not form.is_submitted():
        r = sendRequest(method="get", url="/unverified-seism/" + str(id), auth=True)
        if r.status_code == 404:
            flash("unverified seism not foun", "danger")
            return redirect(url_for("unverified_seism.index"))

        unverified_seism = json.loads(r.text)
        form.depth.data = unverified_seism["depth"]
        form.magnitude.data = unverified_seism["magnitude"]
        form.verified.data = unverified_seism["verified"]

    # en la siguiente linea se produce la validacion del sismo pasandole los datos del diccionario anterior
    # por eso utilizo [""] diccionario
    if form.validate_on_submit():
        unverified_seism = {
            "depth": form.depth.data,
            "magnitude": form.magnitude.data,
            "verified": form.verified.data,
        }
        data = json.dumps(unverified_seism)
        r = sendRequest(
            method="PUT", url="/unverified-seism/" + str(id), data=data, auth=True
        )
        flash("Unverified Seism was edited", "success")
        return redirect(url_for("unverified_seism.index"))
    return render_template(
        "unverified_seismEdit.html",
        unverified_seism=unverified_seism,
        form=form,
    )
