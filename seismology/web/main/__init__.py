import os
from flask import Flask, flash, redirect, url_for
from dotenv import load_dotenv
from flask_wtf import CSRFProtect
from flask_login import LoginManager
from flask_breadcrumbs import Breadcrumbs

login_manager = LoginManager()


@login_manager.unauthorized_handler
def unauthorized_callback():
    flash("You need to login for continue.", "warning")
    # Redireccionar a la pagina principal para logearse de nuevo.
    return redirect(url_for("main.index"))


def create_app():
    app = Flask(__name__)
    load_dotenv()

    # Configuraciones
    app.config["API_URL"] = os.getenv("API_URL")
    app.config["JWT_ACCESS_TOKEN_EXPIRES"] = 10
    csrf = CSRFProtect(app)
    app.config["SECRET_KEY"] = os.getenv("SECRET_KEY")
    Breadcrumbs(app=app)
    login_manager.init_app(app)

    # registrar los blueprints
    from main.routes import (
        main,
        verified_seism,
        unverified_seism,
        sensors,
        users,
        login,
    )

    app.register_blueprint(routes.main.main)
    app.register_blueprint(routes.verified_seism.verified_seism)
    app.register_blueprint(routes.unverified_seism.unverified_seism)
    app.register_blueprint(routes.sensors.sensors)
    app.register_blueprint(routes.users.users)
    app.register_blueprint(routes.login.login_bp)
    return app
